# devops-netology
1 commit

### Список файлов, которые будут проигнорированны
- все локальные директории .terraform
- файлы с расширением .tfstate
- файлы содержащие .tfstate.
- файлы crash.log
- файлы с расширением .tvars
- файлы override.tf
- файлы override.tf.json
- файлы с окончанием _override.tf
- файлы с окончанием _override.tf.json
- файлы .terraformrc
- файлы terraform.rc

